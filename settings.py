fbdev = "/dev/fb1"

try:
	from STK import FBSurface
	fbmem = FBSurface.open_fbmem(fbdev)
	surface = FBSurface.cairo_surface_from_fbmem(fbmem)
except:
	print("could not access framebuffer %s"%(fbdev))
	surface = FBSurface.cairo_surface_dummy("/tmp/fbe_buffer", 128, 32, 1)

mpdHost = "localhost"
mpdPort = 6600
evdevFile = "/dev/input/event0"
weatherLocation = "Berlin"
defaultSettings = {"homeScreen.mpdSize":12}
