#!/usr/bin/python3 -u

import settings
from STK import STKCore, STKGui
from evdev import ecodes
import time
import crontab
import requests
import os
from subprocess import call
from mpd import MPDClient, ConnectionError
import socket

class ClockWidget(STKGui.TextWidget):
	def __init__(self, width, height, updateDelay=60):
		STKGui.TextWidget.__init__(self, width, height, fontsize=height)
		self.updateTimer = STKCore.SynchronizedTimer(updateDelay)
		self.updateTimer.expired.connect(self._handleTimerExpired)
		STKCore.Application.timerMod.addTimer(self.updateTimer)
	
	def resize(self, width, height):
		self. fontsize = height
		STKGui.Drawable.resize(self, width, height)
		
	def _handleTimerExpired(self):
		self.setText(time.strftime("%H:%M"))
		self.updateRequest.emit(self)
		
class MPDWidget(STKGui.TextWidget):
	
	menuName = "MPD"
				
	def __init__(self, width, height, mpdHost, mpdPort, updateDelay=1):
		STKGui.TextWidget.__init__(self, width, height, fontsize=height)
		self.mpdClient = MPDClient(use_unicode=True)
		self.mpdHost = mpdHost
		self.mpdPort = mpdPort
		self.statusChange = STKCore.Signal([str])
		self.status = ""
		self.currentSong = {}
		self.updateTimer = STKCore.Timer(updateDelay)
		self.updateTimer.expired.connect(self._handleTimerExpired)
		STKCore.Application.timerMod.addTimer(self.updateTimer)
		
		self.actions = [STKCore.FunctionAction('play', self.mpdClient.play),
						STKCore.FunctionAction('pause', self.mpdClient.pause),
						STKCore.FunctionAction('stop', self.mpdClient.stop),
						STKCore.FunctionAction('next', self.mpdClient.next),
						STKCore.FunctionAction('prev', self.mpdClient.previous),
						STKCore.FunctionAction('vol+', self.volUp),
						STKCore.FunctionAction('vol-', self.volDown)
						]
						
		self.properties = [ STKCore.ModuleProperty('MPD Font Size', self.setFontsize, lambda: [8, 10, 16, 32]),
						    STKCore.ModuleProperty('Playlist', self.setSong, self.getSongs, self.getCurrentSong),
						    STKCore.ModuleProperty('Choose Playlist', self.setPlaylist, self.getPlaylists, None)
					]
						
	def resize(self, width, height):
		self. fontsize = height
		STKGui.Drawable.resize(self, width, height)
	
	def setFontsize(self, size):
		self.fontsize = size
	
	def setPlaylist(self, itemName):
		startplayback = True if self.status == "play" else False
		self.mpdClient.clear()
		self.mpdClient.load(itemName)
		if startplayback: self.mpdClient.play()
		print("MPD: Loaded playlist %s"%(itemName))
	
	def getPlaylists(self):
		try:
			return [item['playlist'] for item in self.mpdClient.listplaylists() if 'playlist' in item]
		except ConnectionError:
			return []
		print()
		
	def getSongs(self):
		try:
			songlist = []
			for item in self.mpdClient.playlistinfo():
				for field in ['title', 'artist', 'name', 'id']:
					if field in item:
						songlist.append(item[field])
						break
			return songlist
			#return [item['title'] if 'title' in item else "song %s"%(item['id']) for item in self.mpdClient.playlistinfo()]
		except ConnectionError:
			return []
	
	def getCurrentSong(self):
		return self.currentSong
	
	def setSong(self, itemName):
		print("MPD: Play Song %s"%(itemName))
		pl = self.getSongs()
		if itemName in pl:
			self.mpdClient.play(pl.index(itemName))
	
	def volUp(self, step=5):
		vol = int(self.mpdClient.status()['volume'])
		self.mpdClient.setvol(vol+step)
	
	def volDown(self, step=5):
		vol = int(self.mpdClient.status()['volume'])
		self.mpdClient.setvol(vol-step)
	
	def _mpdConnect(self):
		try:
			self.mpdClient.connect(self.mpdHost, self.mpdPort)
		except:
			pass
		
	def _handleTimerExpired(self):
		try:
			currentSong = self.mpdClient.currentsong()
			state = self.mpdClient.status()['state']
		except (ConnectionError, socket.error):
			self._mpdConnect()
			return
		if state != self.status or self.currentSong != currentSong: 
			self.status = state
			self.currentSong = currentSong
			self.statusChange.emit(state)
			
			text = ""
			if state == "play":
				if 'artist' in currentSong:
					text = "%s - "%(currentSong['artist'])
				if 'title' in currentSong:
					text = "%s%s"%(text,currentSong['title'])
			
			self.setText(text)
			self.updateRequest.emit(self)
			
	def handleKeyEvent(self,event):
		if event.value != 1: return
		if event.code == ecodes.KEY_PLAY:
				self.mpdClient.play()
		if event.code == ecodes.KEY_PAUSE:
				self.mpdClient.pause()
		if event.code == ecodes.KEY_STOP:
				self.mpdClient.stop()
		if event.code == ecodes.KEY_NEXT:
				self.mpdClient.next()
		if event.code == ecodes.KEY_LAST:
				self.mpdClient.previous()
		if event.code == ecodes.KEY_VOLUMEUP:
			self.volUp()
		if event.code == ecodes.KEY_VOLUMEDOWN:
			self.volDown()

class AlarmWidget(STKGui.IconWidget):
	
	_cronComment_ = "pyclock alarm"
	
	def __init__(self, width, height, filename="icons/10x10/Alarm-Clock-icon1.png"):
		STKGui.IconWidget.__init__(self, width, height, filename)
		self.cron = crontab.CronTab(tabfile="cron.tab")
		self.jobOn = None
		for job in self.cron.find_comment("%s on"%self._cronComment_):
			self.jobOn = job
		if self.jobOn == None:	
			self.jobOn = self.cron.new('mpc --host %s play'%(settings.mpdHost), comment="%s on"%self._cronComment_)
			self.jobOn.enable(False)
		
		self.jobOff = None
		for job in self.cron.find_comment("%s off"%self._cronComment_):
			self.jobOff = job
		if self.jobOff == None:	
			self.jobOff = self.cron.new('mpc --host %s stop'%(settings.mpdHost), "%s off"%self._cronComment_)
			self.jobOff.enable(False)
		
		self.enabled = self.jobOn.is_enabled()
		
		self.menuName = "Alarm"
		self.actions = [STKCore.FunctionAction('Alarm On', self.enable),
						STKCore.FunctionAction('Alarm Off', self.disable),
						]
		self.properties = [ STKCore.ModuleProperty('Hour', self.setHour, lambda: range(25), self.getHour),
						    STKCore.ModuleProperty('Minute', self.setMinute, lambda: range(61), self.getMinute),
						  ]
	
	def _updateCron_(self):
		self.cron.write()
		call(("fcrontab cron.tab").split(' '))
			
	def setHour(self, hour):
		self.jobOn.hour.on(hour)
		self.jobOff.hour.on((hour+2)%24)
		self._updateCron_()
		
	def getHour(self):
		return self.jobOn.hour
		
	def setMinute(self, minute):
		self.jobOn.minute.on(minute)
		self.jobOff.minute.on(minute)
		self._updateCron_()

	def getMinute(self):
		return self.jobOn.minute
		
	def enable(self):
		self.jobOn.enable(True)
		self.jobOff.enable(True)
		STKGui.IconWidget.enable(self)
		self._updateCron_()

	def disable(self):
		self.jobOn.enable(False)
		self.jobOff.enable(False)
		STKGui.IconWidget.disable(self)
		self._updateCron_()

	def handleKeyEvent(self,event):
		if event.value != 1: return
		if event.code == ecodes.KEY_RED:
				self.toggle()
				self.update()


class FeedWidget(STKGui.TextWidget):
	def __init__(self, width, height, feedURL, updateDelay = 600, fontsize=8, scrollDelay=0.3):
		STKGui.TextWidget.__init__(self, width, height, fontsize=fontsize, scrollDelay=scrollDelay)
		self.feedURL = feedURL
		self.updateTimer = STKCore.SynchronizedTimer(updateDelay)
		self.updateTimer.expired.connect(self._requestFeedData)
		STKCore.Application.timerMod.addTimer(self.updateTimer)
	
	def _requestFeedData(self):
		try:
			self.feedData = requests.get('https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=%s'%(self.feedURL)).json()
			headlines = [entry['title'] for entry in self.feedData['responseData']['feed']['entries']]
			if self.lines == 1:
					self.setText(" *** ".join(headlines))
			else:
				for i in range(self.lines):
					self.setText(headlines[i], i)
		except:
			print("could not obtain feed data")
			self.feedData = None

class WeatherWidget(STKGui.IconWidget):
	
	menuName = "weather"
	APIKEY = "8993ee37da3283f0086ff15ac21634e2"
	
	def __init__(self, width, height, location,filename="icons/10x10/weather/Unknown.png", updateDelay=600):
		STKGui.IconWidget.__init__(self, width, height, filename)
		self.location = location
		self.updateTimer = STKCore.SynchronizedTimer(updateDelay)
		self.updateTimer.expired.connect(self._requestWeatherData)
		STKCore.Application.timerMod.addTimer(self.updateTimer)
		
		self.properties = [ STKCore.ModuleProperty('Today', None, self.getWeatherDay0, None)
					#STKCore.ModuleProperty('Tomorrow', None, self.getWeatherDay1, None)
					#STKCore.ModuleProperty('Day 2', None, self.getWeatherDay2, None)
					#STKCore.ModuleProperty('Day 3', None, self.getWeatherDay3, None)
				]
		
	def _requestWeatherData(self):
		try:
			self.weatherData = requests.get('http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s'%(self.location,self.APIKEY), timeout=1).json()
			iconfile = "icons/10x10/weather/%s.png"%(self.weatherData['weather'][0]['main'])
			if os.path.exists(iconfile):
				self.setIcon(iconfile)
			else:
				print("weather icon not found: %s"%(iconfile))
				self.setIcon("icons/10x10/weather/Unknown.png")
		except Exception as e:
			print("could not obtain weather data: %s"%(e))
			self.weatherData = None
			self.setIcon("icons/10x10/weather/Unknown.png")
			
	def getWeatherDay0(self):
		if self.weatherData != None:
			return ["Temp: %i deg C"%(self.weatherData['main']['temp']-273.15),
					"Hum: %i percent"%(self.weatherData['main']['humidity']),
					"press: %i hPa"%(self.weatherData['main']['pressure'])
					]
		else:
			return ["No weather data available"]

class SleepWidget(STKGui.TextWidget):
	
	
	menuName = "Sleep Timer"
	
	def __init__(self, width, height, updateDelay=60):
		STKGui.TextWidget.__init__(self, width, height)
		self.enabled = False
		self.expired = STKCore.Signal([])
		self.updateTimer = STKCore.Timer(updateDelay)
		self.updateTimer.expired.connect(self._handleTick)
		STKCore.Application.timerMod.addTimer(self.updateTimer)
		
		self.properties = [ STKCore.ModuleProperty('Minutes', self.setCountdown, lambda: range(0, 135, 15), None)]

	def setCountdown(self, countdownMins):
		self.startTime = time.time()
		self.countdownMins = countdownMins
		self.enabled = True
		self._handleTick()

	def _handleTick(self):
		if self.enabled:
			timeRem = self.startTime + 60*self.countdownMins - time.time()
			self.setText("%i"%(round(timeRem/60.)))
			if timeRem <= 0:
				self.expired.emit()
				self.enabled = False
				self.clearText()
		else:
			self.clearText()

class MenuWidget(STKGui.Widget):
	menu = STKCore.Menu()
	def __init__(self, width=128, height=32):
		STKGui.Widget.__init__(self, width, height)
		layout = STKGui.VBoxLayout()
		
		#build menu canvas
		menu = STKGui.MenuWidget(128, 32, self.menu)
		layout = STKGui.HBoxLayout()
		layout.addWidget(menu)
		self.setRootLayout(layout)
		
		self.leave = menu.leave
		
class HomeWidget(STKGui.Widget):
	def __init__(self, width=128, height=32):
		STKGui.Widget.__init__(self, width, height)
		layout = STKGui.VBoxLayout()

		#build homescreen canvas
		self.clock = ClockWidget(118, 32)
		self.bar1 = AlarmWidget(10, 10, filename="icons/10x10/Alarm-Clock-icon2.png")
		self.bar2 = STKGui.IconWidget(10, 10, "icons/10x10/Basic-Message-icon1.png")
		self.bar2.disable()
		self.bar3 = WeatherWidget(8, 8, settings.weatherLocation)
		
		self.mpdWid = MPDWidget(118,10, settings.mpdHost, settings.mpdPort)
		self.mpdWid.statusChange.connect(self.mpdStatusChange)
		
		#self.feed = FeedWidget(118, 10, "http://www.tagesschau.de/xml/rss2", fontsize=10)
		self.sleep = SleepWidget(12,8)
		self.sleep.expired.connect(self.mpdWid.mpdClient.stop)

		self.barLayout = STKGui.HBoxLayout()
		self.barLayout.addWidget(self.bar1)
		self.barLayout.addWidget(self.sleep)
		self.barLayout.addWidget(self.bar3)
		
		self.clockLayout = STKGui.HBoxLayout()
		self.clockLayout.addWidget(self.clock)
		self.clockLayout.addWidget(self.mpdWid)
		
		layout.addWidget(self.clockLayout)
		layout.addWidget(self.barLayout)
		
		self.setRootLayout(layout)
		
	def mpdStatusChange(self, status):
		print("status:", status)
		if status == "play":
			self.clock.resize(self.clock.width(),32-STKCore.Application.settings['homeScreen.mpdSize'])
			self.mpdWid.resize(self.mpdWid.width(),STKCore.Application.settings['homeScreen.mpdSize'])
		else:
			self.clock.resize(self.clock.width(),32)



if __name__ == "__main__":
	homeScreen = HomeWidget()
	menuScreen = MenuWidget()
	
	for wid in [homeScreen.mpdWid, homeScreen.bar1, homeScreen.bar3, homeScreen.sleep]:
		if wid.menuName:
			submenu = STKCore.Menu()
			for action in wid.actions:
				submenu.addAction(action)
			if len(wid.properties) > 0:
				for prop in wid.properties:
					submenu.addMenu(STKCore.PropertyMenu(prop), prop.name)
			menuScreen.menu.addMenu(submenu, wid.menuName)
	menuScreen.menu.addAction(STKCore.RunCmdAction("Shut down", "/sbin/halt"))
		
	
	sd = STKGui.Display(settings.surface)
	sd.setWidget(homeScreen)
	menuScreen.leave.connect(lambda: sd.setWidget(homeScreen))
	homeScreen.update()
	
	def moveCoursor(event):
		if event.value != 1: return
		if sd.widget == menuScreen:
			if event.code == ecodes.KEY_ESC or event.code == ecodes.KEY_BACK:
				sd.setWidget(homeScreen)
		if sd.widget == homeScreen:
			if event.code == ecodes.KEY_ENTER or event.code == ecodes.KEY_OK:
				sd.setWidget(menuScreen)
		
	app = STKCore.Application(defaultSettings=settings.defaultSettings)
	
	inputMod = STKCore.InputModule(settings.evdevFile)
	inputMod.keyEvent.connect(sd.handleKeyEvent)
	inputMod.keyEvent.connect(moveCoursor)
	
	app.addModule(inputMod)
	app.start()
	
	sd.close()
	
